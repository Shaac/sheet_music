all: $(TARGET).pdf

%.pdf: %.ly
	lilypond $<

clean:
	rm -f *.pdf

.PHONY: all clean
