\version "2.14.2"

% Note position {{{ %
% Depending on the number of used toms, use the follings toms:
% 1: tomfh
% 2: tommh
% 3: tomml
% 4: tomfl
% 5: tomh
% 6: toml
#(define mydrums '(
                   (crashcymbal     cross       #f  6)
                   (ridebell        triangle    #f  5)
                   (hihat           cross       #f  5)
                   (openhihat       xcircle     #f  5)
                   (hightom         default     #f  4)
                   (himidtom        default     #f  3)
                   (lowmidtom       default     #f  2)
                   (snare           default     #f  1)
                   (lowtom          default     #f  0)
                   (highfloortom    default     #f  -1)
                   (lowfloortom     default     #f  -2)
                   (bassdrum        default     #f  -3)))
% }}}

% Layout {{{ %
\header {
    title = "Code Morse"
    subtitle = "Vadrum"
}

\paper {
    oddFooterMarkup = \markup {
        \fill-line {
            \line {
                \fromproperty #'header:title -
                by \fromproperty #'header:subtitle -
                sheet by Shaac using lilypond
            }
        }
    }
}
% }}}

\score {
    \new DrumStaff <<
        \new DrumVoice = "1" { s1 }
        \new DrumVoice = "2" { s1 }
        \drummode {
            \set Staff.beamExceptions = #'()
            \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums)
            \override Staff.TimeSignature #'style = #'()
            \override MultiMeasureRest #'expand-limit = #1
            \compressFullBarRests

            \time 2/4
            << {sn16-"R" sn-"R" sn-"L" sn-"L" sn-"R" sn-"R" sn-"L" sn-"L"} \\
            {s2} >>

            \time 4/4
            << {
                sn16-"R" < \parenthesize sn >-"L" < \parenthesize sn >-"R" bd
                tommh-"R" < \parenthesize sn >-"L" < \parenthesize sn >-"R" bd
                sn-"R" < \parenthesize sn >-"L" sn-"R" < \parenthesize sn >-"L"
                < \parenthesize sn >-"R" bd tommh-"R" sn-"L"
            } \\
            {s1} >>

            << {
                sn16-"R" bd sn-"R" < \parenthesize sn >-"L"
                <\parenthesize sn >-"R" bd tomfh-"R" < \parenthesize sn >-"L"
                < \parenthesize sn >-"R" bd sn-"R" < \parenthesize sn >-"L"
                sn-"R" < \parenthesize sn >-"L" < \parenthesize sn >-"R" bd
            } \\
            {s1} >>

            << {s1} \\ {
                < cymc bd >16-"R" < \parenthesize sn >-"L"
                < \parenthesize sn>-"L" tommh-"R"
                bd sn cymc sn
                tommh bd sn cymc
                sn bd bd sn32 bd
            } >>
        }
    >>

    \layout {}
}

% vim:filetype=lilypond
