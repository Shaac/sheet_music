\version "2.14.2"

#(define mydrums '(
            (crashcymbal     la        #f           5)
            (ridebell        triangle  #f           5)
            (hihat           cross     #f           5)
            (openhihat       xcircle   #f           5)
            (lowmidtom       default   #f           2)
            (bassdrum        default   #f           -5)
            (snare           default   #f           0)
            (highfloortom    default   #f           -3)))

\header {
    title = "Space Unicorn"
        subtitle = "Parry Gripp"
}

\paper {
    oddFooterMarkup = \markup {
        \fill-line {
            \line {
                \fromproperty #'header:title -
                    by \fromproperty #'header:subtitle -
                    sheet by Shaac using lilypond
            }
        }
    }
}

\score {
    \new DrumStaff << 
        \new DrumVoice = "1" { s1 *1 } % voix du haut (une mesure)
        \new DrumVoice = "2" { s1 *1 } % voix du bas (une mesure)
        \drummode { 
            \set Staff.beamExceptions = #'() {}
            \set DrumStaff.drumStyleTable = #(alist->hash-table mydrums) {}
            \override Staff.Clef #'stencil = ##f {}
            \override Staff.TimeSignature #'style = #'() {}
            \override MultiMeasureRest #'expand-limit = #1 {}
            \compressFullBarRests {}
            R1*5 {}

            \repeat percent 3 {<< {r8 hh16 hh hho8 hh16 hh hh hh hh hh hho4} \\ 
                {bd4 r r r8 bd} >>}
            << {r8 hh16 hh hho8 hh16 hh hh hh hh hh s4} \\ 
            {bd4 r r sn16 sn sn sn} >> 

            << {cymc8 hh hh hh hh hh hh hh} \\
            {bd4 sn8 bd16 tomml bd8 bd sn4} >>
            << {hh8 hh hh hh hh hh hh hh} \\
            {bd4 sn8 bd16 tomml bd8 bd sn8 sn} >>
            << {hh8 hh hh hh hh hh hh hh} \\
            {bd4 sn8 bd16 tomml bd8 bd sn4} >>
            << {hh8 hh hh hh s2} \\
            {bd4 sn8 bd16 sn sn8 sn sn16 sn sn sn} >>

            << {cymc8 rb hh rb hh rb hh rb} \\
            {bd4 sn bd sn} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn sn16 sn bd8 sn4} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn bd sn} >>
            << {hh8 rb hh rb hh rb s4} \\
            {bd4 sn sn16 sn bd8 sn16 sn sn sn} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn bd sn} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn sn16 sn bd8 sn4} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn bd8 bd sn4} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn sn16 sn bd8 sn4} >>
            << {hh8 rb hh rb s2} \\
            {bd4 sn sn16 sn sn sn sn sn sn sn} >>

            << {cymc8 rb hh rb hh rb hh rb} \\
            {bd4 sn8 bd16 tomml bd8 bd sn4} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn8 bd16 tomml bd8 bd sn8 sn} >>
            << {hh8 rb hh rb hh rb hh rb} \\
            {bd4 sn8 bd16 tomml bd8 bd sn4} >>
            << {hh8 rb hh rb s2} \\
            {bd4 sn8 bd16 sn sn8 sn sn16 sn sn sn} >>

            \repeat percent 2 << {s2. sn4} \\
            {bd4 bd bd bd}>>
            << {s1} \\
            {bd4 bd bd bd} >>
            \time 2/4
                << {s2} \\
                {bd4 bd} >>
            \time 4/4
                << {cymc4 s2.} \\
                {bd8 tomfh bd tomfh <tomfh bd> bd bd bd} >>
            << {s1} \\
            {bd8 bd bd bd sn16 sn sn sn sn sn sn sn} >>

            s1*0^\segno
                \repeat volta 2 {
                    \repeat percent 3 << {\repeat unfold 8 {hh8}} \\
                    {bd8. bd16 sn4 bd16 bd tomml bd sn8. tomml16} >> }
            \alternative {
                {<< {\repeat unfold 6 {hh8} s4} \\
                    {bd8. bd16 sn4 sn16 bd tomml bd sn16 sn sn sn} >> }
                {<< {hh8 hh hh hh s2} \\
                    {bd8 sn16 bd sn8 sn sn16 sn sn sn sn sn sn sn^\segno} >> }}

            << {\repeat unfold 8 {hh8}} \\
            {bd4 bd bd bd} >>
            << {hh8 hh hh hh hh4 s} \\
            {bd4 bd bd4 r} >>
        } 
    >>

        \layout { }
}
